//
//  AppViajesRegistroViewController.h
//  AppViajesParseUniversal
//
//  Created by Steven on 17/05/14.
//
//

#import <UIKit/UIKit.h>
#import "FXImageView.h"

@interface AppViajesRegistroViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
{
    //Label que muestra las ciudades de origen
    IBOutlet UILabel *lblCiudadOrigen;
    //Picker con la ciudades de origen
    IBOutlet UIPickerView *pckrCiudades;
    //Label que muestra la ciudad de destino
    IBOutlet UILabel *lblCiudadDestino;
    //Picker con ciudades de destino
    IBOutlet UIPickerView *pckrCiudadDestino;
    //Listado de ciudades
    NSArray *datosCiudades;
    //Ids de parse para las ciudades
    NSArray *KeyCiudades;
    UIImage *imageToResize;
}

//Label que contiene la fecha de salida
@property (strong, nonatomic) IBOutlet UILabel *salidaLabel;

//Datepicker que contiene la fecha de salida
@property (strong, nonatomic) IBOutlet UIDatePicker *pickerSalida;

//Evento de cambio de fecha para asignarlo al label
- (IBAction)pickerSalidaCambioFecha:(id)sender;

//Propiedad que apunta a la celda con la fecha de salida
@property (strong, nonatomic) IBOutlet UITableViewCell *celdaFechaSalida;

//Formateador para las fechas
@property (strong, nonatomic) NSDateFormatter* dateFormater;

//Propiedad que almacena la fecha de salida seleccionada
@property (strong, nonatomic) NSDate *fechaSeleccionada;

//Propiedad que determina si se esta mostrando o no la celda de fecha de salida
@property (assign, nonatomic) BOOL *fechaSalidaIsShowing;

//Propiedad que determina si se esta mostrando o no la celda de ciudades
@property (assign, nonatomic) BOOL *pickerCiudadesShowing;

//Celda que contiene los uipicker con las ciudades
@property (strong, nonatomic) IBOutlet UITableViewCell *celdaCiudades;

//Label que muestra la cantidad de cupos
@property (strong, nonatomic) IBOutlet UILabel *txtCupos;

//Accion para cambiar el label del texto segun el valor del stepper
- (IBAction)cambioCupos:(UIStepper *)sender;

//Propiedad para la imagen del vehiculo
@property (strong, nonatomic) IBOutlet FXImageView *imgVehiculo;

//Evento guardar viaje
- (IBAction)guardarViaje:(id)sender;

//Variables para almacenar el key de la ciudad de origen y destino seleccionada
@property (strong, nonatomic) NSString *idCiudadOrigen;
@property (strong, nonatomic) NSString *idCiudadDestino;

//Texto con el valor del viaje
@property (strong, nonatomic) IBOutlet UITextField *txtValorViaje;

//Comentario del viaje
@property (strong, nonatomic) IBOutlet UITextView *txtComentarioViaje;

@property (assign, nonatomic) double *cantidadCuposInt;
@property (strong, nonatomic) IBOutlet UIStepper *stepCupos;

@end
