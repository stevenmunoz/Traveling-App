//
//  AppViajesRegistroViewController.m
//  AppViajesParseUniversal
//
//  Created by Steven on 17/05/14.
//
//

#import "AppViajesRegistroViewController.h"
#import "Ciudad.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MBProgressHUD.h"

@interface AppViajesRegistroViewController ()

@end

@implementation AppViajesRegistroViewController
@synthesize salidaLabel;
@synthesize pickerSalida;
@synthesize celdaFechaSalida;
@synthesize dateFormater;
@synthesize fechaSeleccionada;
@synthesize fechaSalidaIsShowing;
@synthesize txtCupos;
@synthesize pickerCiudadesShowing;
@synthesize imgVehiculo;
@synthesize idCiudadOrigen;
@synthesize idCiudadDestino;
@synthesize txtValorViaje;
@synthesize txtComentarioViaje;
@synthesize cantidadCuposInt;
@synthesize stepCupos;


//Alto para las celdas
#define kDatePickerCellHeight 160
//Posicion de la celda de fecha de salida
#define indexDatePicker 3

//Posicion de la celda de ciudad origen - destino
#define indexPickerCiud 1


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self inicializarFecha];
    [self keyboardWillShow];
    [self hidePickerCityCell];
    //NSArray correspondiente a las ciudades del picker
    datosCiudades = [[NSArray alloc]initWithObjects:@"Armenia", @"Barranquilla", @"Bello", @"Bogotá", @"Bucaramanga", @"Buenaventura", @"Buga", @"Cali", @"Cartagena", @"Cartago", @"Cúcuta", @"Dos Quebradas", @"Envigado", @"Florencia", @"Floridablanca", @"Girardot", @"Giron", @"Ibagué", @"Itagüí", @"Maicao", @"Manizales", @"Medellín", @"Montería", @"Neiva", @"Palmira", @"Pasto", @"Pereira", @"Popayán", @"Santa Marta", @"Sincelejo", @"Soacha", @"Sogamoso", @"Soledad", @"Tuluá", @"Tunja", @"Valledupar", @"Villavicencio", nil];
    
    //Claves correspondientes a parse baas para las ciudades
    KeyCiudades = [[NSArray alloc]initWithObjects:@"iljqpFtlw2", @"dY1e1N7lBX", @"bu5xdjduJy", @"M0PwR0OiLj", @"I6lIUigaMW", @"3pdGEgW11u", @"gyNqq9Df3d", @"96fBw38WjD", @"zU4SgJ4gWg", @"RQL5jRbv28", @"WKeml9TPU2", @"TD2ST7JW3o", @"rbY0TvouDm", @"z8OstPbPf1", @"bpz2ehm2Yo", @"9aUzDOLHaK", @"0FfJkGYbuw", @"HWVankK31Y", @"eVPfZzFVh1", @"eDB52HudE6", @"IxeAOiO20C", @"U7qxaBLc1r", @"p65c69FJYb", @"HMXSqmrqAK", @"vTgrRbwAKm", @"3QnKIRA8YQ", @"6mTgpBWt9x", @"xWBec1cIJN", @"2vZqDToSQH", @"khfZdxun0b", @"RgbxFgjDZK", @"kVorAeitTL", @"4ccnZHNMpD", @"18sQA0AdBr", @"AyOKkVG8yK", @"CN9SfVFuW7", @"kPcAlcIOIN", nil];
    
    pckrCiudades.delegate = self;
    // Do any additional setup after loading the view.
    
    self.imgVehiculo.reflectionScale = 0.1f;
    self.imgVehiculo.reflectionAlpha = 0.5f;
    self.imgVehiculo.reflectionGap = 1.0f;
    self.imgVehiculo.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.imgVehiculo.shadowBlur = 5.0f;
    self.imgVehiculo.cornerRadius = 10.0f;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Action para cambiar el texto seleccionado por el del UIDatePicker
- (IBAction)pickerSalidaCambioFecha:(UIDatePicker *)sender {
    
    self.salidaLabel.text = [self.dateFormater stringFromDate:sender.date];
    self.fechaSeleccionada = sender.date;
}

//Inicializa la fecha de salida y la fecha minima con la fecha actual
- (void) inicializarFecha {
    
    self.dateFormater = [[NSDateFormatter alloc] init];
    [self.dateFormater setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormater setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *fechaActual = [NSDate date];
    self.salidaLabel.text = [self.dateFormater stringFromDate:fechaActual];
    self.salidaLabel.textColor = [self.tableView tintColor];
    self.fechaSeleccionada = fechaActual;
    self.pickerSalida.minimumDate = fechaActual;
    
}

//Para mostrar y ocultar la celda que tiene la fecha haremos su alto 0
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    //Si es la seccion de informacion del viaje
    if (indexPath.section == 0)
    {
        self.pickerSalida.hidden = YES;
        //Permite ocultar o mostrar la celda de fecha de salida
        if (indexPath.row == indexDatePicker){
            height = self.fechaSalidaIsShowing ? kDatePickerCellHeight : 0;
        }
        
        //Permite ocultar o mostrar la celda del picker de ciudad origen y destino
        if (indexPath.row == indexPickerCiud)
        {
            height = self.pickerCiudadesShowing ? kDatePickerCellHeight : 0;
        }
    }
    //Si es la seccion de informacion del vehiculo
    else
    {
        //Alto para la celda de captura de foto
        if(indexPath.row == 2)
        {
            height = 160;
        }
    }
 
    return height;
}

//Determinamos si la fecha de salida se esta mostrando u ocultado
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0)
    {
    
        //Si se selecciona la celca con el texto origen - destino determinamos si ocultar o mostrar el picker de ciudades
        if (indexPath.row == 0){
        
            if (self.pickerCiudadesShowing){
            
                [self hidePickerCityCell];
            
            }else {
            
                [self showPickerCityCell];
            }
        }
    
        //Si se selecciona la celda con el texto fecha determinamos si ocultar o mostrar el datepicker
        if (indexPath.row == 2){
        
            if (self.fechaSalidaIsShowing){
            
                [self hideDatePickerCell];
            
            }else {
            
                [self showDatePickerCell];
            }
        }
    
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    //Si selecciona el campo para cargar fotos
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 2)
        {
            [self showPhotoLibary];
        }
    }
}

//Muestra el datepicker de salida
- (void)showDatePickerCell {
    
    BOOL b = YES;
    self.fechaSalidaIsShowing = &b;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.pickerSalida.hidden = NO;
    self.celdaFechaSalida.hidden = NO;
    self.pickerSalida.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.pickerSalida.alpha = 1.0f;
        
    }];
}

//Oculta el datepicker de salida
- (void)hideDatePickerCell {
    
    self.fechaSalidaIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.pickerSalida.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.pickerSalida.hidden = YES;
                     }];
}

//Muestra el picker de ciudades
- (void)showPickerCityCell {
    
    BOOL b = YES;
    self.pickerCiudadesShowing = &b;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    pckrCiudades.hidden = NO;
    pckrCiudadDestino.hidden = NO;
    self.celdaCiudades.hidden = NO;
    pckrCiudadDestino.alpha = 0.0f;
    pckrCiudades.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        pckrCiudadDestino.alpha = 1.0f;
        pckrCiudades.alpha = 1.0f;
    }];
}

//Oculta el picker de ciudades
- (void)hidePickerCityCell {
    
    self.pickerCiudadesShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         pckrCiudadDestino.alpha = 0.0f;
                         pckrCiudades.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         pckrCiudadDestino.hidden = YES;
                         pckrCiudades.hidden = YES;
                     }];
}

//Evitamos que cuando este el UIDatePicker se muestre el teclado
- (void)signUpForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillShow {
    
    if (self.fechaSalidaIsShowing){
        [self hideDatePickerCell];
    }
}

//Cantidad de pickers a mostrarse
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//Cantidad de elementos en los picker view
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [datosCiudades count];
}

//Textos para los picker view, ordenamos el diccionario alfabeticamente
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //Obtenemos el id de la ciudad dado su nombre, ya que al ordenarse alfabeticamente se pierde la referencia al elemento
    //NSArray *temp = [dict allKeysForObject:[nombres objectAtIndex:row]];
    //NSString *key = [temp objectAtIndex:0];
   
    if (component == 0)
    {
        lblCiudadOrigen.text = [datosCiudades objectAtIndex:row];
        self.idCiudadOrigen = [KeyCiudades objectAtIndex:row];
    }
    else if (component == 1)
    {
        lblCiudadDestino.text = [datosCiudades objectAtIndex:row];
        self.idCiudadDestino = [KeyCiudades objectAtIndex:row];
    }
    
}

//Modificamos el font size de los picker view
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        tView.font = [UIFont fontWithName:@"System" size:14];
    }
    
    tView.text = [datosCiudades objectAtIndex:row];
    return tView;
}

//Evento del stepper para cambiar el label segun aumente o disminuya el valor
- (IBAction)cambioCupos:(UIStepper *)sender {
    
    double valor = [sender value];
    self.txtCupos.text = [NSString stringWithFormat:@"%d", (int)valor];
}

//Evento para mostrar la galeria de fotos
- (void)showPhotoLibary
{
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Displays saved pictures from the Camera Roll album.
    mediaUI.mediaTypes = @[(NSString*)kUTTypeImage];
    
    // Hides the controls for moving & scaling pictures
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = self;
    
   [self presentViewController:mediaUI animated:YES completion:nil];
   
}

//Delegado para manejar el evento de seleccion de fotos de la camara
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo:
(NSDictionary *) info {
    
    //UIImagePickerControllerOriginalImage si la edicion esta inhabilitada
    UIImage *image = info[UIImagePickerControllerEditedImage];
    //UIImage *originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
    self.imgVehiculo.image = image;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//Permite ocultar el teclado cuando se presiona la tecla return, debe implementar el delegado del textfield
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//Registro de un viaje
- (IBAction)guardarViaje:(id)sender {
    
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    //hud.dimBackground = YES;
    hud.labelText = @"Registrando";
    [hud show:YES];
    
    PFObject *viaje = [PFObject objectWithClassName:@"viaje"];
    
    //Usuario del viaje
    viaje[@"personaViaje"] = [PFObject objectWithoutDataWithClassName:@"_User" objectId:@"nap6sChhAU"];
    
    //Ciudad de origen
    viaje[@"origen"] = [PFObject objectWithoutDataWithClassName:@"ciudad" objectId:self.idCiudadOrigen];
    
    //Ciudad de destino
    viaje[@"destino"] = [PFObject objectWithoutDataWithClassName:@"ciudad" objectId:self.idCiudadDestino];
    
    //Formato del precio
    NSDecimalNumber *num = [NSDecimalNumber decimalNumberWithString:self.txtValorViaje.text locale:NSLocale.currentLocale];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    //Valor del viaje
    viaje[@"valorViaje"] = [formatter stringFromNumber:num];
    
    //Cupos disponibles
    viaje[@"cuposViaje"] = [NSNumber numberWithDouble:self.stepCupos.value];
    
    //Imagen del vehiculo
    imageToResize = [self scaleImage:self.imgVehiculo.image scaledToSize:CGSizeMake(350, 350)];
  
    NSData *imgFinal = UIImageJPEGRepresentation(imageToResize, 0.7);
    NSString *filename = [NSString stringWithFormat:@"%@.jpg", @"imgVehiculo"];
    
    PFFile *imageFile = [PFFile fileWithName:filename data:imgFinal];
    viaje[@"imagen"] = imageFile;
    
    //Fecha de salida
    viaje[@"fechaSalidaViaje"] = self.fechaSeleccionada;
    
    //Comentarios del conductor
    viaje[@"comentarioViaje"] = self.txtComentarioViaje.text;
    
    [viaje saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        [hud hide:YES];
        
        if (!error) {
            // Show success message
            MBProgressHUD *hudComplete = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            hudComplete.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            
            // Set custom view mode
            hudComplete.mode = MBProgressHUDModeCustomView;
            
            hudComplete.labelText = @"Completado";
            [hudComplete show:YES];
            [hudComplete hide:YES afterDelay:3];
            
            // Notify table view to reload the recipes from Parse cloud
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
            
            // Dismiss the controller
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ha ocurrido un error, inténtelo en unos minutos" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [hud hide:YES];
            
        }
        
    }];
    
}

- (UIImage *)scaleImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    float ratio = newSize.width/image.size.width;
    [image drawInRect:CGRectMake(0, 0, newSize.width, ratio * image.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    //NSLog(@"New Image Size : (%f, %f)", newImage.size.width, newImage.size.height);
    UIGraphicsEndImageContext();
    return newImage;
}


@end
