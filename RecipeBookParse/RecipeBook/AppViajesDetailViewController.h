//
//  RecipeDetailViewController.h
//  RecipeBook
//
//  Created by Simon Ng on 17/6/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Viaje.h"

@interface AppViajesDetailViewController : UIViewController
{
    IBOutlet UIScrollView *scroller;
}

@property (weak, nonatomic) NSString *idViaje;
@property (weak, nonatomic) IBOutlet PFImageView *imagenVehiculo;
@property (weak, nonatomic) IBOutlet UILabel *precio;
@property (weak, nonatomic) IBOutlet UILabel *origen;
@property (weak, nonatomic) IBOutlet UITextView *comentario;
@property (weak, nonatomic) IBOutlet PFImageView *fotoConductor;
@property (weak, nonatomic) IBOutlet UILabel *nombreConductor;
@property (weak, nonatomic) IBOutlet UILabel *telefonoConductor;
@property (weak, nonatomic) IBOutlet UILabel *fechaSalida;

@property (nonatomic, strong) Viaje *viaje;

@property (strong, nonatomic) IBOutlet UIButton *registroViajeClick;


@end
