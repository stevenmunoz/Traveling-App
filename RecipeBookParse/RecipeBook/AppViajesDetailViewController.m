//
//  RecipeDetailViewController.m
//  RecipeBook
//
//  Created by Simon Ng on 17/6/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import "AppViajesDetailViewController.h"
#import "MBProgressHUD.h"
#import "QuartzCore/QuartzCore.h"

@interface AppViajesDetailViewController ()

@end

@implementation AppViajesDetailViewController

@synthesize idViaje;
@synthesize imagenVehiculo;
@synthesize precio;
@synthesize origen;
@synthesize comentario;
@synthesize fotoConductor;
@synthesize nombreConductor;
@synthesize telefonoConductor;
@synthesize fechaSalida;
@synthesize viaje;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [scroller setScrollEnabled:YES];
    [scroller setContentSize:CGSizeMake(320, 920)];
    
    self.fotoConductor.layer.cornerRadius = 15;
    //self.fotoConductor.layer.borderWidth = 1;
    //self.fotoConductor.layer.borderColor = [UIColor blackColor].CGColor;
    self.fotoConductor.clipsToBounds = YES;
    
    self.imagenVehiculo.file = viaje.imagenVehiculo;
    self.precio.text =  [NSString stringWithFormat:@"%@  %@", viaje.cupos, viaje.precio];
    self.origen.text = [NSString stringWithFormat:@"%@ - %@", viaje.origen, viaje.destino];
    self.nombreConductor.text = viaje.nombreConductor;
    self.telefonoConductor.text = viaje.telefonoConductor;
    self.fotoConductor.file = viaje.imagenConductor;
    //foto del conductor
    NSString *imageUrl = viaje.urlImagenConductor;
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        self.fotoConductor.image = [UIImage imageWithData:data];
    }];
    
    //[self.urlConductor setImageWithURL:[NSURL URLWithString:[viaje.urlImagenConductor]];
    
    self.comentario.text = viaje.comentarios;
    self.fechaSalida.text = viaje.fechaSalida;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)registroViaje:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    //hud.dimBackground = YES;
    hud.labelText = @"Registrando";
    [hud show:YES];
    
    //Enviamos los datos a parse
    PFObject *registroViaje = [PFObject objectWithClassName:@"registro_viaje"];
    
    //Buscamos el usuario con el id del usuario actual
    registroViaje[@"idPersona"] = [PFObject objectWithoutDataWithClassName:@"_User" objectId:@"nap6sChhAU"];
    //Buscamos el objeto con el id del viaje correspondiente
    registroViaje[@"idViaje"] = [PFObject objectWithoutDataWithClassName:@"viaje" objectId:viaje.idViaje];
    //Tipo de persona viajero
    registroViaje[@"tipoPersona"] = @"V";
    //Estado solicitud de registro
    registroViaje[@"estadoPersona"] = @"S";
    
    [registroViaje saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [hud hide:YES];
        
        if (!error) {
            // Show success message
            MBProgressHUD *hudComplete = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            hudComplete.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            
            // Set custom view mode
            hudComplete.mode = MBProgressHUDModeCustomView;
            
            hudComplete.labelText = @"Completado";
            [hudComplete show:YES];
            
            [hudComplete hide:YES afterDelay:3];
            
            // Notify table view to reload the recipes from Parse cloud
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
            
            // Dismiss the controller
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ha ocurrido un error, inténtelo en unos minutos" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [hud hide:YES];
        }
        
    }];
    
}


@end
