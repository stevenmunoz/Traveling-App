//
//  RecipeBookViewController.m
//  RecipeBook
//
//  Created by Simon Ng on 26/7/13.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import "AppViajesViewController.h"
#import "AppViajesDetailViewController.h"
#import "Viaje.h"

@interface AppViajesViewController ()

@end

@implementation AppViajesViewController {
    
}

static NSDateFormatter *sUserVisibleDateFormatter = nil;

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"viaje";
        
        // The key of the PFObject to display in the label of the default cell style
        //self.textKey = @"valorViaje";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 15;
    }
    return self;
}


- (void)viewDidLoad
{
    if (![PFUser currentUser])
    {
        //PFLogInViewController *login = [[PFLogInViewController alloc] init];
    }
    
    //Remove the rows separator, our images already come with separator
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    //Styling the table background
    self.parentViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"common_bg"]];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    //Padding from top
    //UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, 0, 0);
    //self.tableView.contentInset = inset;
    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) viewDidAppear:(BOOL)animated
{
    PFLogInViewController *loginViewController = [[PFLogInViewController alloc] init];
    [self presentViewController:loginViewController animated:YES completion:NULL];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (PFQuery *)queryForTable
{
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    [query includeKey:@"origen"];
    [query includeKey:@"destino"];
    [query includeKey:@"personaViaje"];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByDescending:@"fechaSalidaViaje"];
    
    return query;
}

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
    static NSString *simpleTableIdentifier = @"cellViajes";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    // Assign our own background image for the cell
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    // Configure the cell
    PFFile *thumbnail = [object objectForKey:@"imagen"];
    PFImageView *thumbnailImageView = (PFImageView*)[cell viewWithTag:100];
    thumbnailImageView.image = [UIImage imageNamed:@"placeholder.jpg"];
    thumbnailImageView.file = thumbnail;
    [thumbnailImageView loadInBackground];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm:ss a"];
    
    //Para devolver datos relacionados debemos obtener el puntero a la tabla
    PFObject *ciudadOrigen = object[@"origen"];
    PFObject *ciudadDestino = object[@"destino"];
    PFObject *personaViaje = object[@"personaViaje"];
    
    //Precio del viaje
    UILabel *precio = (UILabel *) [cell viewWithTag:101];
    precio.text = [NSString stringWithFormat:@"$%@ ", [object objectForKey:@"valorViaje"]];
    
    //Origen
    UILabel *origenDestino = (UILabel *) [cell viewWithTag:102];
    origenDestino.text = [NSString stringWithFormat:@"%@ - %@", ciudadOrigen[@"nombre"], ciudadDestino[@"nombre"]];
    
    //Fecha de salida
    NSString *theDate = [dateFormatter stringFromDate:[object objectForKey:@"fechaSalidaViaje"]];
    //NSDate *fechaNueva = theDate;
    UILabel *fecha = (UILabel *) [cell viewWithTag:104];
    fecha.text = [NSString stringWithFormat:@"%@",theDate];
    
    //Cupos
    UILabel *cupos = (UILabel *) [cell viewWithTag:105];
    cupos.text = [NSString stringWithFormat:@"Cupos: %@", [object objectForKey:@"cuposViaje"]];
    
    //Conductor
    UILabel *conductor = (UILabel *) [cell viewWithTag:106];
    conductor.text = [NSString stringWithFormat:@"Conduce: %@", personaViaje[@"nombres"]];
    
    return cell;
}

- (void) objectsDidLoad:(NSError *)error
{
    [super objectsDidLoad:error];
    
    //NSLog(@"error: %@", [error localizedDescription]);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"detalleViaje"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        AppViajesDetailViewController *destViewController = segue.destinationViewController;
        
        PFObject *object = [self.objects objectAtIndex:indexPath.row];
        Viaje *viaje = [[Viaje alloc] init];
        viaje.imagenVehiculo = [object objectForKey:@"imagen"];
        
        //Origen
        PFObject *ciudadOrigen = object[@"origen"];
        viaje.origen = ciudadOrigen[@"nombre"];
        
        //Destino
        PFObject *ciudadDestino = object[@"destino"];
        viaje.destino = ciudadDestino[@"nombre"];
        
        //Precio
        viaje.precio = [NSString stringWithFormat:@"$%@ ", [object objectForKey:@"valorViaje"]];
        
        //Comentarios
        viaje.comentarios = [object objectForKey:@"comentarioViaje"];
        
        //Cupos
        viaje.cupos = [NSString stringWithFormat:@"Cupos:%@", [object objectForKey:@"cuposViaje"]];
        
        //Fecha de Salida
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm:ss a"];
        NSString *fechaFormato = [dateFormatter stringFromDate:[object objectForKey:@"fechaSalidaViaje"]];
        viaje.fechaSalida = [NSString stringWithFormat:@"%@",fechaFormato];
        
        //Foto conductor
        PFObject *personaViaje = object[@"personaViaje"];
        PFFile *imagenFisica = personaViaje[@"imagen"];
        //NSLog(@"%@", imagenFisica.url);
        viaje.imagenConductor = personaViaje[@"imagen"];
        viaje.urlImagenConductor = imagenFisica.url;
        
        //Nombre conductor
        viaje.nombreConductor = personaViaje[@"nombres"];
        
        //Telefono conductor
        viaje.telefonoConductor = [NSString stringWithFormat:@"Tel: %@", personaViaje[@"telefono"]];
        
        //Asignamos el id del viaje
        viaje.idViaje = [object objectId];
        
        destViewController.viaje = viaje;
        
    }
}

//Fondos de celdas personalizados
- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"cell_top.png"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"cell_bottom.png"];
    } else {
        background = [UIImage imageNamed:@"cell_middle.png"];
    }
    
    return background;
}

// Override to customize the look of the cell that allows the user to load the next page of objects.
// The default implementation is a UITableViewCellStyleDefault cell with simple labels.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"NextPage";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.text = @"Ver Más...";
    
    return cell;
}

//Cachar la conversion para eficiciencia PROBAR
- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString {
    /*
     Returns a user-visible date time string that corresponds to the specified
     RFC 3339 date time string. Note that this does not handle all possible
     RFC 3339 date time strings, just one of the most common styles.
     */
    
    // If the date formatters aren't already set up, create them and cache them for reuse.
    static NSDateFormatter *sRFC3339DateFormatter = nil;
    if (sRFC3339DateFormatter == nil) {
        sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [sRFC3339DateFormatter dateFromString:rfc3339DateTimeString];
    
    NSString *userVisibleDateTimeString;
    if (date != nil) {
        if (sUserVisibleDateFormatter == nil) {
            sUserVisibleDateFormatter = [[NSDateFormatter alloc] init];
            [sUserVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
            [sUserVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
        }
        // Convert the date object to a user-visible date string.
        userVisibleDateTimeString = [sUserVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}



@end
