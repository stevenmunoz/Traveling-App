//
//  Ciudad.h
//  AppViajesParseUniversal
//
//  Created by Steven on 17/05/14.
//
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Ciudad : NSObject

@property (nonatomic, strong) NSString *idCiudad;
@property (nonatomic, strong) NSString *nombre;

@end
