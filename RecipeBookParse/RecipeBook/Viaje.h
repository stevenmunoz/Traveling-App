//
//  Recipe.h
//  RecipeBook
//
//  Created by Simon on 12/8/12.
//
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Viaje : NSObject

@property (nonatomic, strong) NSString *idViaje;
@property (nonatomic, strong) NSString *origen;
@property (nonatomic, strong) NSString *destino;
@property (nonatomic, strong) PFFile *imagenVehiculo;
@property (nonatomic, strong) NSString *fechaSalida;
@property (nonatomic, strong) NSString *comentarios;
@property (nonatomic, strong) NSString *precio;
@property (nonatomic, strong) PFFile *imagenConductor;
@property (nonatomic, strong) NSString *nombreConductor;
@property (nonatomic, strong) NSString *telefonoConductor;
@property (nonatomic, strong) NSString *cupos;
@property (nonatomic, strong) NSString *urlImagenConductor;

@end
