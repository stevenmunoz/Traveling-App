//
//  Recipe.m
//  RecipeBook
//
//  Created by Simon on 12/8/12.
//
//

#import "Viaje.h"

@implementation Viaje

@synthesize idViaje;
@synthesize origen;
@synthesize destino;
@synthesize imagenVehiculo;
@synthesize fechaSalida;
@synthesize precio;
@synthesize nombreConductor;
@synthesize telefonoConductor;
@synthesize comentarios;
@synthesize cupos;
@synthesize imagenConductor;

@end
